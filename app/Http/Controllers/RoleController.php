<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class RoleController extends Controller
{
    //

    public function isAdmin(){

        return view('admin');
    }

    public function isFunder(){

        return view('funder');
    }

    public function isTrainer(){

        return view('trainer');
    }

    public function isCM(){

        return view('cm');
    }
}
