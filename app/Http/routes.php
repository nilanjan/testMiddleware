<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('admins', ['middleware' => 'admin', 'uses' => 'RoleController@isAdmin']);

Route::get('funders', ['middleware' => 'funder', 'uses' => 'RoleController@isFunder']);

Route::get('trainers', ['middleware' => 'trainer', 'uses' => 'RoleController@isTrainer']);

Route::get('cm', ['middleware' => 'cm', 'uses' => 'RoleController@isCM']);


Route::auth();

Route::get('/home', 'HomeController@index');
