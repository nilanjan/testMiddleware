<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;
use Auth;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function accessAdmin(){

        $role = DB::table('users')
                ->where('id', Auth::user()->id)
				->select('role_id')
                ->get();
        //dd($role[0]->role_id);
		
		$role = $role[0]->role_id;
		
		if(($role == 1) || ($role == 2)){
			
			return true;
		}
		else{
			
			return false;	
		}
		        
    }

    public function isTrainer(){

		$roles = DB::table('roles_tr')
    				->where('user_id', Auth::user()->id)
    				->get(['role_id']);
		
		//dd($roles);

    	foreach ($roles as $role) {
		    $r = $role->role_id;

		    if(($r == 1) || ($r == 2) || ($r == 3)){
				
				return true;
			}
			
		}		
			return false;
        
    }
	
	public function isFunder(){
		
		
	$role = DB::table('users')
	                ->where('id', Auth::user()->id)
					->select('role_id')
	                ->get();
			
			//dd($role[0]->role_id);
			
			$role = $role[0]->role_id;
			
			if(($role == 1) || ($role == 2) || ($role == 4)){
				
				return true;
			}
			else{
				
				return false;	
			}
    }

    public function accessCM(){

    	$roles = DB::table('roles_tr')
    				->where('user_id', Auth::user()->id)
    				->get(['role_id']);

    	//dd($roles);

    	foreach ($roles as $role) {
		    $r = $role->role_id;

		    if(($r == 1) || ($r == 2) || ($r == 4)){
				
				return true;
			}
			
		}		
			return false;
    }
	
}
